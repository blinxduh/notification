local function MountApp(app: ScreenGui) 
    if gethui() then 
        app.Parent = gethui()
    elseif syn.protect_gui then
        app.Parent = game:GetService("CoreGui")
        syn.protect_gui(app)
    else
        app.Parent = game:GetService("CoreGui")
    end
end

local App = game:GetObjects("rbxassetid://13902747445")[1]

local suc, err = pcall(MountApp, App)
if err then error(err) end

local Notifications = App.Notification

Notifications.Notification.Visible = false

local function CloneNewNotification() 
    return Notifications.Notification:Clone()
end

local Interaction = {}

local NOTIFICATION_COUNT = 0

function Interaction:PostNotification(Options) 
    local Options = Options or {
        Title = Options.Title or "Title",
        Message = Options.Message or "Message",
        Duration = Options.Duration or 5,
        OnClose = Options.OnClose or nil,
        Buttons = Options.Buttons or nil
    }

    NOTIFICATION_COUNT += 1

    local newNotification = CloneNewNotification()
    newNotification.Visible = true
    newNotification.LayoutOrder = NOTIFICATION_COUNT

    newNotification.Title.Text = Options.Title
    newNotification.Message.Text = Options.Message
    
    if Options.Buttons ~= nil or Options.Buttons then
        newNotification.Buttons.Container.Button.Visible = false

        local function CloneNewButton() 
            return newNotification.Buttons.Container.Button:Clone()
        end

        for i, button in ipairs(Options.Buttons) do 
            local newButton = CloneNewButton()
            newButton.Parent = newNotification.Buttons.Container
            newButton.Visible = true

            newButton.TextButton.Text = button.Text
            newButton.Size = UDim2.new(0,newButton.TextButton.TextBounds.X + 10,1,-10)

            newButton.MouseEnter:Connect(function()
                game:GetService("TweenService"):Create(newButton, TweenInfo.new(.2), {
                    BackgroundColor3 = Color3.fromRGB(45, 45, 45)
                }):Play()
            end)

            newButton.MouseLeave:Connect(function()
                game:GetService("TweenService"):Create(newButton, TweenInfo.new(.2), {
                    BackgroundColor3 = Color3.fromRGB(39, 39, 39)
                }):Play()
            end)

            newButton.TextButton.Activated:Connect(function()
                local suc, err = pcall(button.Callback)
                if err then error(err) end
            end)
        end
    else
        newNotification.Buttons.Visible = false
        newNotification.Size = UDim2.new(0,newNotification.Size.X.Offset,0,newNotification.Size.Y.Offset-27)
    end

    -- newNotification.Title.returnNotification.Activated:Connect(function()
    --     game:GetService("TweenService"):Create(newNotification, TweenInfo.new(.5), {
	-- 		BackgroundTransparency = 1
	-- 	}):Play()

    --     for _, obj in ipairs(newNotification:GetDescendants()) do 
	-- 		if obj:IsA("GuiObject") and obj ~= nil then
	-- 			if obj:IsA("Frame") then
	-- 				game:GetService("TweenService"):Create(obj, TweenInfo.new(.5), {
	-- 					BackgroundTransparency = 1
	-- 				}):Play()
	-- 			end

	-- 			if obj:IsA("ImageLabel") or obj:IsA("ImageButton") then
	-- 				game:GetService("TweenService"):Create(obj, TweenInfo.new(.5), {
	-- 					ImageTransparency = 1
	-- 				}):Play()
	-- 			end

	-- 			if obj:IsA("TextLabel") or obj:IsA("TextButton") then
	-- 				game:GetService("TweenService"):Create(obj, TweenInfo.new(.5), {
	-- 					TextTransparency = 1
	-- 				}):Play()
	-- 			end
	-- 		end
	-- 	end
		
	-- 	task.wait(.3)
	-- 	NOTIFICATION_COUNT -= 1
	-- 	newNotification:Destroy()

    --     if Options.OnClose then 
    --         local suc, err = pcall(Options.OnClose)
    --         if err then error(err) end
    --     end
    -- end)

    coroutine.wrap(function() 
        task.wait(Options.Duration)

        game:GetService("TweenService"):Create(newNotification, TweenInfo.new(.5), {
			BackgroundTransparency = 1
		}):Play()

        for _, obj in ipairs(newNotification:GetDescendants()) do 
			if obj:IsA("GuiObject") and obj ~= nil then
				if obj:IsA("Frame") then
					game:GetService("TweenService"):Create(obj, TweenInfo.new(.5), {
						BackgroundTransparency = 1
					}):Play()
				end

				if obj:IsA("ImageLabel") or obj:IsA("ImageButton") then
					game:GetService("TweenService"):Create(obj, TweenInfo.new(.5), {
						ImageTransparency = 1
					}):Play()
				end

				if obj:IsA("TextLabel") or obj:IsA("TextButton") then
					game:GetService("TweenService"):Create(obj, TweenInfo.new(.5), {
						TextTransparency = 1
					}):Play()
				end
			end
		end
		
		task.wait(.5)
		NOTIFICATION_COUNT -= 1
		newNotification:Destroy()

        if Options.OnClose then 
            local suc, err = pcall(Options.OnClose)
            if err then error(err) end
        end
    end)()
end

return Interaction